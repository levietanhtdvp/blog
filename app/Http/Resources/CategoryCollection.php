<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'id' => $this->id,
                'name' => $this->name,
                // 'childs' => CategoryResource::collection($this->whenLoaded('childs')),
                'status' => $this->status,
                'updated_at' => $this->updated_at,
            ]
        ];
    }
}
