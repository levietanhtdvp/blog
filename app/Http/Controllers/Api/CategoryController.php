<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Category as ResourcesCategory;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Exception;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *@param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $category = Category::where('parent_id',0)->with('childs')->get();
        // if($request->id !=null){
        //     $category = CategoryResource::collection(Category::where('parent_id',0)->get());

        // }
        $category = CategoryResource::collection(Category::where('parent_id', 0)->orderby('id','DESC')->get());
        return response()->json([
            'data' => $category,
            'code' => Response::HTTP_OK
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $category = new Category();
            $addcat =  $category->saveData($request);
            return response()->json([
                'data' => $addcat,
                'message' => 'Create new category successfully!',
                'code' => Response::HTTP_OK
            ]);
        } 
        catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ]);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $category = Category::find($id);
            return response()->json([
                'data' => $category,
                'code' => Response::HTTP_OK
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $cat_del = Category::find($id);
            if ($cat_del == null) {
                return response()->json([
                    'message' => 'Category code does not exist in the system!',
                    'code' => 404
                ]);
            } else {
                $cat_del->childs()->delete();
                $cat_del->delete();
                return response()->json([
                    'message' => 'Delete Category successfully!',
                    'code' => 200
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ]);
        }
    }
}
